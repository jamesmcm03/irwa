# Assumptions

I'm assuming that you are somewhat comfortable with Rust. This is not an
introduction to Rust and I won't go into the details of any syntax or
meaning of code that is presented. With that said, we won't be using any
hardcore Rust language features so you should be able to follow along if
you're still a Rust beginner.

I'm assuming that you have general knowledge about web architecture at a
high level: how databases work, what an ORM is, what a REST API is, and how
HTML, CSS, and JavaScript interact in the browser. I'll go over details for
the specific tools you're using -- you don't need to be an SQL or
JavaScript expert, for example.

I'm assuming that you have access to a platform that is supported by Rust
and the other tools used in this book. I've used Ubuntu 18.04 to prepare
and test all of the code that will be presented, along with current
nightly Rust builds and current releases of all crates presented as of the
time of this writing (August 2019).
