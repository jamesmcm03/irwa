# Introduction

This book provides a taste of the full-stack, all-Rust approach to building web
apps.

The world doesn't need another todo app, but we're going to make one anyway
because it's become something of a tradition, and because it's easily
understood.

We will walk through the layers of the application, starting at the bottom
of the stack with the database. We will set up a database file, using
SQLite3 as our database engine.

Then we will write a database access library that the higher layers will
build on. For our convenience as developers, and for debugging and
troubleshooting purposes, we will build cli-driven programs that use the
database access layer to read and write the database. This is generally
more convenient -- and safer -- than modifying or querying the database
directly.

The next layer is the REST API. This will be built on Rocket, which is a Rust
web framework. Our frontend client will send HTTP requests to our Rocket
program. When Rocket calls our code, we will use the database access layer to
read or write the database. The REST API code will massage the data into an
appropriate format (JSON) before returning it to the client.

The top layer, or frontend, is the Web UI that we present to the user. This runs
in a web browser as WebAssembly (JavaScript). We will use the Seed framework to
compile our Rust code into a WebAssembly app that we can load into the browser.

Finally we'll look at some of the limitations of this simple app, discuss
improvements, and provide some pointers to resources that can help in
addressing those issues.
