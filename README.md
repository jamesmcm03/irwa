This repo contains the source to "Introduction to Rust Web Applications"
from https://erwabook.com -- the book is best read there. (Generating the book
from the source here is unsupported.)

Please open issues or merge requests for any errors that you find as you read.

Thanks!

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

# Changes

* 2019-09-04
    * migrate from bookdown to [mdbook](https://rust-lang-nursery.github.io/mdBook/index.html)
    * minor text improvements
    * add `mdbook build` to the ci pipeline

* 2019-08-31
    * extracted IRWA to its own repo, share on gitlab
    * various corrections from r/rust/ feedback
    * tested walkthrough in fresh 18.04 container; added more package installation notes

* 2019-08-30 -- Initial public release
