#[macro_use]
extern crate diesel;
// ANCHOR: serde
#[macro_use]
extern crate serde;
// ANCHOR_END: serde

pub mod db;
